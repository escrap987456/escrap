package com.jason.escrap.Database;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;


import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.jason.escrap.Activity.HomeActivity;
import com.jason.escrap.Fragments.LoginFragment;

import com.jason.escrap.Model.Alert;
import com.jason.escrap.Model.Users;
import com.jason.escrap.R;
import com.jason.escrap.Services.GpsTracker;
import com.jason.escrap.Utils.Utils;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class Registration {
    Context context;
    DatabaseReference databaseReference;
    Alert alert;
    ProgressDialog pDialog;
    SharedPreferences.Editor editor;
    Double longitude, latitude;
    GpsTracker gpsTracker;


    public Registration(Context context) {
        this.context = context;
        alert = new Alert(context);
        pDialog = new ProgressDialog(context);
        pDialog.setMessage("Please wait...");
        pDialog.setCancelable(false);
        editor = context.getSharedPreferences("userInfo", context.MODE_PRIVATE).edit();
        gpsTracker = new GpsTracker(context);

    }


    public void register_user(FirebaseAuth firebaseAuth, String name, String email, String password, String address, String phoneno, String description, String uid,
                              String product_owner, String product_seller, String general) {
        pDialog.show();
        firebaseAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    final FirebaseUser firebaseUser = firebaseAuth.getCurrentUser();
                    if (firebaseUser != null) {
                        firebaseUser.sendEmailVerification().addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    if (ContextCompat.checkSelfPermission(context,
                                            android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                                        longitude = gpsTracker.getLongitude();
                                        latitude = gpsTracker.getLongitude();
                                        //    Log.d("Tag",longitude.toString()+" "+latitude.toString());
                                    }
                                    new SaveUserInfo().execute(address, description, email, general, latitude.toString(), longitude.toString(), name, phoneno, product_owner, product_seller, firebaseAuth.getUid());

                                } else {
                                    pDialog.dismiss();
                                    alert.error_alert("Error", "Registration failed,Please try again");
                                }
                            }
                        });
                    }
                } else {
                    pDialog.dismiss();
                    alert.error_alert("Error", "Email already registered");
                }

            }
        });

    }

    public void show_login_fragment() {
        FragmentTransaction fragmentTransaction = ((AppCompatActivity) context).getSupportFragmentManager().beginTransaction();
        Fragment bf = new LoginFragment();
        fragmentTransaction.replace(R.id.intro_activity_container, bf);
        fragmentTransaction.commit();
    }

    public void firebaseAuthWithGoogle(FirebaseAuth firebaseAuth, String name, String email, String password, String address, String phoneno, String description, String uid,
                                       String dog_owner, String dog_breeder, String trainer, String token) {
        pDialog.show();
        AuthCredential credential = GoogleAuthProvider.getCredential(token, null);
        firebaseAuth.signInWithCredential(credential).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                boolean isNewUser = task.getResult().getAdditionalUserInfo().isNewUser();
                if (isNewUser) {
                    if (task.isSuccessful()) {
                        if (ContextCompat.checkSelfPermission(context,
                                android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                            longitude = gpsTracker.getLongitude();
                            latitude = gpsTracker.getLatitude();
                        }
                        databaseReference = FirebaseDatabase.getInstance().getReference("User").child(firebaseAuth.getUid());
                        Users users = new Users(name, email, address, phoneno, description, firebaseAuth.getUid(), dog_owner, dog_breeder, trainer, latitude, longitude);

                        databaseReference.setValue(users).addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                pDialog.dismiss();
                                alert.success_alert("Success", "Successfully registered");
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                pDialog.dismiss();
                                alert.error_alert("Error", "Registration failed,Please try again");
                            }
                        });
                    } else {
                        pDialog.dismiss();
                        alert.error_alert("Error", "Registration failed,Please try again");
                    }
                } else {
                    pDialog.dismiss();
                    context.startActivity(new Intent(context.getApplicationContext(), HomeActivity.class));
                }
            }
        });
    }

    public void firebaseloginwithGoogle(FirebaseAuth firebaseAuth, String token) {
        pDialog.show();
        AuthCredential credential = GoogleAuthProvider.getCredential(token, null);
        firebaseAuth.signInWithCredential(credential).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                boolean isNewUser = task.getResult().getAdditionalUserInfo().isNewUser();
                if (isNewUser) {
                    if (task.isSuccessful()) {

                        FirebaseUser user = firebaseAuth.getCurrentUser();
                        if (ContextCompat.checkSelfPermission(context,
                                android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                            longitude = gpsTracker.getLongitude();
                            latitude = gpsTracker.getLongitude();
                        }
                        new SaveGmailUserInfo().execute("", "", user.getEmail(), "0", latitude.toString(), longitude.toString(), user.getDisplayName(), "", "0", "0", firebaseAuth.getUid());
                    } else {
                        pDialog.dismiss();
                        alert.error_alert("Error", "Registration failed,Please try again");
                    }
                } else {
                    pDialog.dismiss();
                    context.startActivity(new Intent(context.getApplicationContext(), HomeActivity.class));
                }
            }
        });
    }

    public void checkEmailverification(FirebaseAuth firebaseAuth) {
        FirebaseUser firebaseUser = firebaseAuth.getInstance().getCurrentUser();
        Boolean emailflag = firebaseUser.isEmailVerified();
        if (emailflag) {
            //show new activity
            pDialog.dismiss();
            context.startActivity(new Intent(context.getApplicationContext(), HomeActivity.class));
        } else {
            pDialog.dismiss();
            alert.error_alert("Email not Verified", "Please verify your email");
            firebaseAuth.signOut();
        }
    }

    public class SaveUserInfo extends AsyncTask<String, Integer, Response> {

        @Override
        protected Response doInBackground(String... strings) {
            OkHttpClient client = new OkHttpClient().newBuilder()
                    .build();
            MediaType mediaType = MediaType.parse("text/plain");
            RequestBody body = new MultipartBody.Builder().setType(MultipartBody.FORM)
                    .addFormDataPart("address", strings[0])
                    .addFormDataPart("description", strings[1])
                    .addFormDataPart("email", strings[2])
                    .addFormDataPart("general", strings[3])
                    .addFormDataPart("lattitude", strings[4])
                    .addFormDataPart("longitude", strings[5])
                    .addFormDataPart("name", strings[6])
                    .addFormDataPart("phoneno", strings[7])
                    .addFormDataPart("productowner", strings[8])
                    .addFormDataPart("productseller", strings[9])
                    .addFormDataPart("uid", strings[10])
                    .build();
            Request request = new Request.Builder()
                    .url(Utils.REGISTRATION_URL)
                    .method("POST", body)
                    .build();
            Response response = null;
            try {
                response = client.newCall(request).execute();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response;
        }

        @Override
        protected void onPostExecute(Response response) {
            super.onPostExecute(response);
            if (response != null) {
                if (response.code() == 200) {
                    pDialog.dismiss();
                    alert.success_alert("Success", "Successfully registered, Please verify your email");
                    show_login_fragment();
                } else {
                    pDialog.dismiss();
                    alert.error_alert("Error", "Registration failed,Please try again");
                }
            } else {
                pDialog.dismiss();
                alert.error_alert("Error", "Registration failed,Please try again");
            }

        }
    }

    public class SaveGmailUserInfo extends AsyncTask<String, Integer, Response> {

        @Override
        protected Response doInBackground(String... strings) {
            OkHttpClient client = new OkHttpClient().newBuilder()
                    .build();
            MediaType mediaType = MediaType.parse("text/plain");
            RequestBody body = new MultipartBody.Builder().setType(MultipartBody.FORM)
                    .addFormDataPart("address", strings[0])
                    .addFormDataPart("description", strings[1])
                    .addFormDataPart("email", strings[2])
                    .addFormDataPart("general", strings[3])
                    .addFormDataPart("lattitude", strings[4])
                    .addFormDataPart("longitude", strings[5])
                    .addFormDataPart("name", strings[6])
                    .addFormDataPart("phoneno", strings[7])
                    .addFormDataPart("productowner", strings[8])
                    .addFormDataPart("productseller", strings[9])
                    .addFormDataPart("uid", strings[10])
                    .build();
            Request request = new Request.Builder()
                    .url(Utils.REGISTRATION_URL)
                    .method("POST", body)
                    .build();
            Response response = null;
            try {
                response = client.newCall(request).execute();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response;
        }

        @Override
        protected void onPostExecute(Response response) {
            super.onPostExecute(response);
            if (response != null) {
                if (response.code() == 200) {
                    pDialog.dismiss();
                    Toast.makeText(context, "Successfully registered", Toast.LENGTH_SHORT).show();
                    context.startActivity(new Intent(context, HomeActivity.class));
                } else {
                    pDialog.dismiss();
                    alert.error_alert("Error", "Registration failed,Please try again");
                }
            } else {
                pDialog.dismiss();
                alert.error_alert("Error", "Registration failed,Please try again");
            }

        }
    }
}
