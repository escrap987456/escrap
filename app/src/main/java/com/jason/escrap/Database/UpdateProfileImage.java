package com.jason.escrap.Database;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;


import com.jason.escrap.Utils.Utils;

import java.io.File;
import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class UpdateProfileImage {
    ProgressDialog pDialog;
    Context context;

    public UpdateProfileImage(Context context){
        this.context=context;
        pDialog = new ProgressDialog(context);
        pDialog.setMessage("Please wait...");
        pDialog.setCancelable(false);
    }


    public void updateProfileCover(String uid, String imagepath) {
        pDialog.show();
        File file = new File(imagepath);
        new updateCoverImage().execute(uid,file.getName(),file.getPath());
    }

    public void updateProfileDP(String uid, String imagepath) {
        pDialog.show();
        File file = new File(imagepath);
        new updateDPImage().execute(uid,file.getName(),file.getPath());
    }

    public class updateCoverImage extends AsyncTask<String, Integer, Response> {

        @Override
        protected Response doInBackground(String... strings) {
            OkHttpClient client = new OkHttpClient().newBuilder()
                    .build();
            MediaType mediaType = MediaType.parse("text/plain");
            RequestBody body = new MultipartBody.Builder().setType(MultipartBody.FORM)
                    .addFormDataPart("uid", strings[0])
                    .addFormDataPart("image_file", strings[1],
                            RequestBody.create(MediaType.parse("application/octet-stream"),
                                    new File(strings[2])))
                    .build();
            Request request = new Request.Builder()
                    .url(Utils.UPDATE_PROFILE_COVER)
                    .method("POST", body)
                    .build();
            Response response = null;
            try {
                response = client.newCall(request).execute();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response;
        }

        @Override
        protected void onPostExecute(Response response) {
            super.onPostExecute(response);

            if (response != null) {
                if (response.code() == 200) {
                    pDialog.dismiss();
                    Toast.makeText(context, "Cover Image updated Successfully,Swipe to refresh", Toast.LENGTH_LONG).show();
                } else {
                    pDialog.dismiss();
                    Toast.makeText(context, "Update failed, Check your internet connection", Toast.LENGTH_SHORT).show();
                }
            } else {
                pDialog.dismiss();
                Toast.makeText(context, "No response from server", Toast.LENGTH_SHORT).show();
            }
        }
    }
    public class updateDPImage extends AsyncTask<String, Integer, Response> {

        @Override
        protected Response doInBackground(String... strings) {
            OkHttpClient client = new OkHttpClient().newBuilder()
                    .build();
            MediaType mediaType = MediaType.parse("text/plain");
            RequestBody body = new MultipartBody.Builder().setType(MultipartBody.FORM)
                    .addFormDataPart("uid", strings[0])
                    .addFormDataPart("image_file", strings[1],
                            RequestBody.create(MediaType.parse("application/octet-stream"),
                                    new File(strings[2])))
                    .build();
            Request request = new Request.Builder()
                    .url(Utils.UPDATE_PROFILE_DP)
                    .method("POST", body)
                    .build();
            Response response = null;
            try {
                response = client.newCall(request).execute();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response;
        }

        @Override
        protected void onPostExecute(Response response) {
            super.onPostExecute(response);
            if (response != null) {
                if (response.code() == 200) {
                    pDialog.dismiss();
                    Toast.makeText(context, "Profile Image updated Successfully,Swipe to refresh", Toast.LENGTH_LONG).show();
                } else {
                    pDialog.dismiss();
                    Toast.makeText(context, "Update failed, Check your internet connection", Toast.LENGTH_SHORT).show();
                }
            } else {
                pDialog.dismiss();
                Toast.makeText(context, "No response from server", Toast.LENGTH_SHORT).show();
            }
        }
    }


}
