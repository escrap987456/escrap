package com.jason.escrap.Database;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;


import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.jason.escrap.Model.Alert;
import com.jason.escrap.Model.Products;
import com.jason.escrap.Services.GpsTracker;
import com.jason.escrap.Utils.Utils;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class UserPosts {
    Context context;
    Alert alert;
    ProgressDialog pDialog;
    FirebaseAuth firebaseAuth;
    GpsTracker gpsTracker;
    Double longitude, latitude;
    MyPostsCallback listener;
    GetAllPostsCallback getAllPostsCallback;
    public UserPosts(Context context, MyPostsCallback listener,GetAllPostsCallback getAllPostsCallback) {
        this.context = context;
        alert = new Alert(context);
        pDialog = new ProgressDialog(context);
        pDialog.setMessage("Please wait...");
        pDialog.setCancelable(false);
        this.listener = listener;
        this.getAllPostsCallback=getAllPostsCallback;
        firebaseAuth = FirebaseAuth.getInstance();
        gpsTracker=new GpsTracker(context);

    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void upload_new_post(String type, String caption, String producttype, String manufacturingDate, String Expirydate,
                                String briefhistory, String warranty, String lendingavailability, String lendingavailability_price,
                                String saleavailability, String saleavailabilityprice, String imagePath, String uid, FirebaseAuth firebaseAuth) {

        if (ContextCompat.checkSelfPermission(context,
                android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            longitude = gpsTracker.getLongitude();
            latitude = gpsTracker.getLatitude();
        }

        pDialog.show();
        File file = new File(imagePath);

        new AddPost().execute(briefhistory, caption, Expirydate, file.getName(),
                file.getPath(), latitude.toString(), lendingavailability,
                lendingavailability_price, longitude.toString(), manufacturingDate,
                producttype, saleavailability, saleavailabilityprice, type, firebaseAuth.getUid(), warranty);
    }

    public void FetchMyPosts() {
        pDialog.show();
        new GetMyPosts(listener).execute();
    }
    public void FetchAllPosts(){
        pDialog.show();
        new GetAllPosts(getAllPostsCallback).execute();
    }


    public class AddPost extends AsyncTask<String, Integer, Response> {

        @Override
        protected Response doInBackground(String... strings) {

            OkHttpClient client = new OkHttpClient().newBuilder()
                    .build();
            MediaType mediaType = MediaType.parse("text/plain");
            RequestBody body = new MultipartBody.Builder().setType(MultipartBody.FORM)
                    .addFormDataPart("briefhistory", strings[0])
                    .addFormDataPart("caption", strings[1])
                    .addFormDataPart("expirydate", strings[2])
                    .addFormDataPart("image_file", strings[3],
                            RequestBody.create(MediaType.parse("application/octet-stream"),
                                    new File(strings[4])))
                    .addFormDataPart("lattitude", strings[5])
                    .addFormDataPart("lendavailability", strings[6])
                    .addFormDataPart("lendavailabilityprice", strings[7])
                    .addFormDataPart("longitude", strings[8])
                    .addFormDataPart("manufacturingdate", strings[9])
                    .addFormDataPart("producttype", strings[10])
                    .addFormDataPart("saleavailability", strings[11])
                    .addFormDataPart("saleavailabilityprice", strings[12])
                    .addFormDataPart("type", strings[13])
                    .addFormDataPart("uid", strings[14])
                    .addFormDataPart("warranty", strings[15])
                    .build();
            Request request = new Request.Builder()
                    .url(Utils.ADD_POST_URL)
                    .method("POST", body)
                    .build();
            Response response = null;
            try {
                response = client.newCall(request).execute();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response;
        }

        @Override
        protected void onPostExecute(Response response) {
            super.onPostExecute(response);
            if (response != null) {
                if (response.code() == 200) {
                    pDialog.dismiss();
                    Toast.makeText(context, "Posted Successfully, Swipe down to refresh", Toast.LENGTH_SHORT).show();
                    ((Activity) context).finish();
                } else {
                    Toast.makeText(context, "Post failed, Check your internet connection", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(context, "No response from server", Toast.LENGTH_SHORT).show();
            }
        }
    }


    public class GetMyPosts extends AsyncTask<String, Integer, Response> {

        MyPostsCallback listener;

        public GetMyPosts(MyPostsCallback listener) {
            this.listener = listener;
        }

        @Override
        protected Response doInBackground(String... strings) {
            OkHttpClient client = new OkHttpClient().newBuilder()
                    .build();
            MediaType mediaType = MediaType.parse("text/plain");
            RequestBody body = new MultipartBody.Builder().setType(MultipartBody.FORM)
                    .addFormDataPart("uid", firebaseAuth.getUid())
                    .build();
            Request request = new Request.Builder()
                    .url(Utils.FETCH_POSTS_URL)
                    .method("POST", body)
                    .build();
            Response response = null;
            try {
                response = client.newCall(request).execute();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response;
        }

        @Override
        protected void onPostExecute(Response response) {
            super.onPostExecute(response);
            if (response != null) {
                pDialog.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    listener.onMyPostsResultReceived(jsonObject);
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }
    }

    public interface MyPostsCallback {
        void onMyPostsResultReceived(JSONObject jsonObject);
    }


    public class GetAllPosts extends AsyncTask<String, Integer, Response> {
        GetAllPostsCallback getAllPostsCallback;

        public GetAllPosts(GetAllPostsCallback getAllPostsCallback) {
            this.getAllPostsCallback = getAllPostsCallback;
        }

        @Override
        protected Response doInBackground(String... strings) {
            OkHttpClient client = new OkHttpClient().newBuilder()
                    .build();
            MediaType mediaType = MediaType.parse("text/plain");
            MediaType JSON = MediaType.parse("application/json; charset=utf-8");
            RequestBody body = RequestBody.create(JSON, "{}");
            Request request = new Request.Builder()
                    .url(Utils.FETCH_POSTS_URL)
                    .method("POST", body)
                    .build();
            Response response = null;
            try {
                response = client.newCall(request).execute();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response;
        }

        @Override
        protected void onPostExecute(Response response) {
            super.onPostExecute(response);
            if (response != null) {
                pDialog.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    getAllPostsCallback.onAllPostsResultReceived(jsonObject);
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }
    }

    public interface GetAllPostsCallback {
        void onAllPostsResultReceived(JSONObject jsonObject);
    }


}
