package com.jason.escrap.Database;

import android.os.AsyncTask;
import android.util.Log;

import com.jason.escrap.Utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;


import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ProfileData {
    String uid;
    TestTaskCallback listener;

    public ProfileData(String uid, TestTaskCallback listener) {
        this.uid = uid;
        this.listener = listener;
    }

    public void getProfileInformation() {
        new RetrieveProfileInfo(listener).execute();
    }


    public class RetrieveProfileInfo extends AsyncTask<String, Integer, Response> {

        TestTaskCallback listener;

        public RetrieveProfileInfo(TestTaskCallback listener) {
            this.listener = listener;
        }

        @Override
        protected Response doInBackground(String... strings) {

            OkHttpClient client = new OkHttpClient().newBuilder()
                    .build();
            MediaType mediaType = MediaType.parse("text/plain");
            RequestBody body = new MultipartBody.Builder().setType(MultipartBody.FORM)
                    .addFormDataPart("uid", uid)
                    .build();
            Request request = new Request.Builder()
                    .url(Utils.PROFILE_INFO_URL)
                    .method("POST", body)
                    .build();
            Response response = null;
            try {
                response = client.newCall(request).execute();

            } catch (IOException e) {
                e.printStackTrace();
            }

            return response;
        }

        @Override
        protected void onPostExecute(Response response) {
            super.onPostExecute(response);
            if (response != null) {
                JSONObject Jobject = null;
                try {
                    Jobject = new JSONObject(response.body().string());
                    JSONArray Jarray = Jobject.getJSONArray("data");

                    JSONObject object = null;
                    for (int i = 0; i < Jarray.length(); i++) {
                        object = Jarray.getJSONObject(i);
                    }
                    listener.onResultReceived(object);
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public interface TestTaskCallback {
        void onResultReceived(JSONObject result);
    }
}
