package com.jason.escrap.Adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.jason.escrap.Activity.ProductDetailsActivity;
import com.jason.escrap.Model.Products;
import com.jason.escrap.Model.Users;
import com.jason.escrap.R;
import com.jason.escrap.Utils.Utils;

import java.io.IOException;
import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class HomeProductAdapter extends RecyclerView.Adapter<HomeProductAdapter.MyViewHolder> implements Filterable {
    Context context;
    ArrayList<Products> productsList;
    ArrayList<Products> exampleproductslist;
    SharedPreferences.Editor editor;
    ArrayList<Users> usersList = new ArrayList<>();

    public HomeProductAdapter(Context context, ArrayList<Products> productsList) {
        this.context = context;
        this.productsList = productsList;
        this.exampleproductslist = productsList;
        editor = context.getSharedPreferences("filter_details", Context.MODE_PRIVATE).edit();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View listViewItem = inflater.inflate(R.layout.home_product_posts_layout_ticket, null, true);
        return new HomeProductAdapter.MyViewHolder(listViewItem);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.post_caption.setText(exampleproductslist.get(position).getCaption());
        holder.dog_type.setText(exampleproductslist.get(position).getProducttype());
        Glide.with(context).load(Utils.FETCH_IMAGE_URL + exampleproductslist.get(position).getImageurl())
                .into(holder.post_image);

        get_owner_details(exampleproductslist.get(position).getUid(), holder, position);

        //if user click on the item
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ProductDetailsActivity.class);
                intent.putExtra("post_caption", exampleproductslist.get(position).getCaption());
                intent.putExtra("product_type", exampleproductslist.get(position).getType());
                intent.putExtra("manufacturingDate", exampleproductslist.get(position).getManufacturingdate());
                intent.putExtra("available_lending", exampleproductslist.get(position).getLendavailability());
                intent.putExtra("available_sale", exampleproductslist.get(position).getSaleavailability());
                intent.putExtra("lending_price", exampleproductslist.get(position).getLendavailability_price());
                intent.putExtra("sale_price", exampleproductslist.get(position).getSaleavailabilityprice());
                intent.putExtra("brief_intro", exampleproductslist.get(position).getBriefhistory());
                intent.putExtra("image_url", Utils.FETCH_IMAGE_URL + exampleproductslist.get(position).getImageurl());
                intent.putExtra("owner_id", exampleproductslist.get(position).getUid());
                context.startActivity(intent);
            }
        });


    }

    @Override
    public int getItemCount() {
        return exampleproductslist.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                String charSequenceString = constraint.toString();
                if (charSequenceString.isEmpty()) {
                    exampleproductslist = productsList;
                } else {
                    ArrayList<Products> filteredList = new ArrayList<>();
                    for (Products name : productsList) {
                        if (name.getCaption().toLowerCase().contains(charSequenceString.toLowerCase())) {
                            filteredList.add(name);
                        }
                        exampleproductslist = filteredList;
                    }
                }
                FilterResults results = new FilterResults();
                results.values = exampleproductslist;
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                exampleproductslist = (ArrayList<Products>) results.values;
                notifyDataSetChanged();
            }
        };
    }

    public void performFiltering(String lending_availability, String sale_availability, String product_type,
                                 String lending_price, String sale_price, String warranty) {
        if (lending_availability.equals("no") && sale_availability.equals("no") &&
                product_type.equals("") &&
                lending_price.equals("") && sale_price.equals("") && warranty.equals("no_selection")
        ) {
            exampleproductslist = productsList;
            editor.remove("lending_availability_status");
            editor.remove("sales_availability_status");
            editor.remove("product_type_status");
            editor.remove("warranty");
            editor.remove("product_lending_price");
            editor.remove("product_sale_price");
            editor.commit();
            notifyDataSetChanged();
        } else {
            ArrayList<Products> filteredList = new ArrayList<>();
            for (Products product : productsList) {
                if (lending_availability.equalsIgnoreCase("yes")) {
                    if (!filteredList.contains(product)) {
                        Log.d("Tag", product.getLendavailability().toLowerCase());
                        if (product.getLendavailability().toLowerCase().equals(lending_availability)) {
                            filteredList.add(product);
                        }
                    }
                    editor.putString("lending_availability_status", "yes");
                } else {
                    editor.remove("lending_availability_status");
                }

                if (sale_availability.equalsIgnoreCase("yes")) {
                    if (!filteredList.contains(product)) {
                        if (product.getSaleavailability().toLowerCase().equals(sale_availability)) {
                            filteredList.add(product);
                        }
                    }
                    editor.putString("sales_availability_status", "yes");
                } else {
                    editor.remove("sales_availability_status");
                }

                if (!product_type.isEmpty()) {
                    if (product.getProducttype().toLowerCase().contains(product_type.toLowerCase())) {
                        if (!filteredList.contains(product)) {
                            filteredList.add(product);
                        }
                    }
                    editor.putString("product_type_status", product_type);
                }

                if (!lending_price.isEmpty()) {
                    int product_price= Integer.parseInt(product.getLendavailability_price());
                    int user_entered_price= Integer.parseInt(lending_price);
                    if(product_price<user_entered_price){
                        if (!filteredList.contains(product)) {
                            filteredList.add(product);
                        }
                    }
                    editor.putString("product_lending_price", lending_price);
                }
                if (!sale_price.isEmpty()) {
                    int product_price= Integer.parseInt(product.getSaleavailabilityprice());
                    int user_entered_price= Integer.parseInt(sale_price);
                    if(product_price<user_entered_price){
                        if (!filteredList.contains(product)) {
                            filteredList.add(product);
                        }
                    }
                    editor.putString("product_sale_price", sale_price);
                }

                if (!warranty.equals("no_selection")) {
                    if (product.getWarranty().toLowerCase().equals(warranty.toLowerCase())) {
                        if (!filteredList.contains(product)) {
                            filteredList.add(product);
                        }
                    }
                    editor.putString("warranty", warranty);
                }
                editor.commit();
                exampleproductslist = filteredList;
                notifyDataSetChanged();
            }
        }
        notifyDataSetChanged();

    }


    public static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView post_caption, dog_type, post_owner_distance;
        ImageView post_image;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            post_image = itemView.findViewById(R.id.post_image);
            post_caption = itemView.findViewById(R.id.txt_post_caption);
            dog_type = itemView.findViewById(R.id.post_dog_type);
            post_owner_distance = itemView.findViewById(R.id.posted_user_distance);
        }
    }

    public void get_owner_details(String uid, MyViewHolder holder, int poition) {
        RetrieveProfileInfo retrieveProfileInfo = new RetrieveProfileInfo(holder, poition);
        retrieveProfileInfo.execute(uid);

    }

    //fetching owner details for each product
    public class RetrieveProfileInfo extends AsyncTask<String, Integer, Response> {

        public MyViewHolder holder;
        public int position;

        public RetrieveProfileInfo(MyViewHolder holder, int position) {
            this.holder = holder;
            this.position = position;
        }

        @Override
        protected Response doInBackground(String... strings) {

            OkHttpClient client = new OkHttpClient().newBuilder()
                    .build();
            MediaType mediaType = MediaType.parse("text/plain");
            RequestBody body = new MultipartBody.Builder().setType(MultipartBody.FORM)
                    .addFormDataPart("uid", strings[0])
                    .build();
            Request request = new Request.Builder()
                    .url(Utils.PROFILE_INFO_URL)
                    .method("POST", body)
                    .build();
            Response response = null;
            try {
                response = client.newCall(request).execute();

            } catch (IOException e) {
                e.printStackTrace();
            }

            return response;
        }

        @Override
        protected void onPostExecute(Response response) {
            super.onPostExecute(response);
            if (response != null) {
                JSONObject Jobject = null;
                try {
                    Jobject = new JSONObject(response.body().string());
                    JSONArray Jarray = Jobject.getJSONArray("data");
                    JSONObject object = null;
                    for (int i = 0; i < Jarray.length(); i++) {
                        object = Jarray.getJSONObject(i);
                        usersList.add(new Users(object.getString("name"), object.getString("email"),
                                object.getString("address"), object.getString("phoneno"), object.getString("description"),
                                object.getString("uid"), object.getString("productowner"), object.getString("productseller"),
                                object.getString("general"), Double.parseDouble(object.getString("lattitude")), Double.parseDouble(object.getString("longitude"))));
                    }

                    try {
                        getKmFromLatLong(usersList.get(0).getLattitude(), usersList.get(0).getLongitude(), productsList.get(position).getLattitude(), productsList.get(position).getLongitude(), holder);
                    } catch (Exception e) {

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static double getKmFromLatLong(double lat1, double lng1, double lat2, double lng2, MyViewHolder holder) {
        Location loc1 = new Location("");
        loc1.setLatitude(lat1);
        loc1.setLongitude(lng1);
        Location loc2 = new Location("");
        loc2.setLatitude(lat2);
        loc2.setLongitude(lng2);
        double distanceInMeters = loc1.distanceTo(loc2);
        holder.post_owner_distance.setText("" + Math.round(distanceInMeters / 1000) + " km away");
        return distanceInMeters / 1000;
    }
}
