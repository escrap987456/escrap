package com.jason.escrap.Utils;

public class Utils {
    public static String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    public static String MapKey = "AIzaSyCspwimsU14pSIBzbDHIRAU37TodLPDqfc";
    public static String REGISTRATION_URL = "https://e-scrap.me/escrap/register.php";
    public static String PROFILE_INFO_URL = "https://e-scrap.me/escrap/selectId.php";
    public static String ADD_POST_URL = "https://e-scrap.me/escrap/AddNewPost.php";
    public static String FETCH_POSTS_URL = "https://e-scrap.me/escrap/FetchPosts.php";
    public static String FETCH_IMAGE_URL = "https://e-scrap.me/escrap/uploads/";
    public static String FETCH_AllUsers_URL = "https://e-scrap.me/escrap/selectAllUsers.php";
    public static String UPDATE_PROFILE_URL="https://e-scrap.me/escrap/updateProfile.php";
    public static String UPDATE_PROFILE_COVER="https://e-scrap.me/escrap/updateProfileCover.php";
    public static String UPDATE_PROFILE_DP="https://e-scrap.me/escrap/updateProfileDP.php";
    public static String FETCH_COVER_IMAGE_URL = "https://e-scrap.me/escrap/ProfileImagesCover/";
    public static String FETCH_DP_IMAGE_URL = "https://e-scrap.me/escrap/ProfileImagesDP/";
}
