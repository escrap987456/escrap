package com.jason.escrap.Fragments.chat.users.getall;


import android.os.AsyncTask;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.jason.escrap.Model.Message_users;
import com.jason.escrap.Model.Users;
import com.jason.escrap.Utils.Utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


public class GetUsersInteractor implements GetUsersContract.Interactor {
    private static final String TAG = "GetUsersInteractor";
    FirebaseAuth firebaseAuth;
    private List<String> userslist;
    private GetUsersContract.OnGetAllUsersListener mOnGetAllUsersListener;
    FirebaseUser fuser;
    DatabaseReference reference;
    private List<Users> users;

    public GetUsersInteractor(GetUsersContract.OnGetAllUsersListener onGetAllUsersListener) {
        this.mOnGetAllUsersListener = onGetAllUsersListener;
    }


    @Override
    public void getAllUsersFromFirebase() {

        userslist = new ArrayList<>();
        fuser = FirebaseAuth.getInstance().getCurrentUser();
        reference = FirebaseDatabase.getInstance().getReference("Message_users");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                userslist.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Message_users message_users = snapshot.getValue(Message_users.class);
                    if (message_users.getSenderUid().equals(fuser.getUid())) {
                        userslist.add(message_users.getReceiverUid());
                    }
                    if (message_users.getReceiverUid().equals(fuser.getUid())) {
                        userslist.add(message_users.getSenderUid());
                    }
                }
                readchats();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }

    public void readchats() {
        users = new ArrayList<>();
        new getAllUsers().execute();
    }

    @Override
    public void getChatUsersFromFirebase() {

    }

    public class getAllUsers extends AsyncTask<String, Integer, Response> {

        @Override
        protected Response doInBackground(String... strings) {
            OkHttpClient client = new OkHttpClient().newBuilder()
                    .build();
            MediaType mediaType = MediaType.parse("text/plain");
            RequestBody body = RequestBody.create(mediaType, "");
            Request request = new Request.Builder()
                    .url(Utils.FETCH_AllUsers_URL)
                    .method("POST", body)
                    .build();
            Response response = null;
            try {
                response = client.newCall(request).execute();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response;
        }

        @Override
        protected void onPostExecute(Response response) {
            super.onPostExecute(response);
            List<Users> AllUsers;
            if (response != null) {
                JSONObject Jobject = null;
                try {
                    Jobject = new JSONObject(response.body().string());
                    JSONArray Jarray = Jobject.getJSONArray("data");
                    JSONObject object = null;
                    AllUsers=new ArrayList<>();
                    AllUsers.clear();
                    users.clear();
                    for (int i = 0; i < Jarray.length(); i++) {
                        object = Jarray.getJSONObject(i);
                        AllUsers.add(new Users(object.getString("name"), object.getString("email"),
                                object.getString("address"), object.getString("phoneno"),
                                object.getString("description"),
                                object.getString("uid"),
                                object.getString("productowner"),
                                object.getString("productseller"),
                                object.getString("general"),
                                Double.parseDouble(object.getString("lattitude")),
                                Double.parseDouble(object.getString("longitude"))));

                        for (String id : userslist) {
                            if (AllUsers.get(i).getUid().equals(id)) {
                                if (users.size() != 0) {
                                    for (Users user1 : users) {
                                        if (!AllUsers.get(i).getUid().equals(user1.getUid())) {
                                            users.add(new Users(object.getString("name"), object.getString("email"),
                                                    object.getString("address"), object.getString("phoneno"),
                                                    object.getString("description"),
                                                    object.getString("uid"),
                                                    object.getString("productowner"),
                                                    object.getString("productseller"),
                                                    object.getString("general"),
                                                    Double.parseDouble(object.getString("lattitude")),
                                                    Double.parseDouble(object.getString("longitude"))));
                                        }
                                    }
                                } else {
                                    users.add(new Users(object.getString("name"), object.getString("email"),
                                            object.getString("address"), object.getString("phoneno"),
                                            object.getString("description"),
                                            object.getString("uid"),
                                            object.getString("productowner"),
                                            object.getString("productseller"),
                                            object.getString("general"),
                                            Double.parseDouble(object.getString("lattitude")),
                                            Double.parseDouble(object.getString("longitude"))));
                                }
                            }
                        }
                        mOnGetAllUsersListener.onGetAllUsersSuccess(users);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }


}
