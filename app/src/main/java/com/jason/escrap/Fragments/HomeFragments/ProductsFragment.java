package com.jason.escrap.Fragments.HomeFragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.jason.escrap.Adapter.PostedProductsAdapter;
import com.jason.escrap.Database.UserPosts;
import com.jason.escrap.Model.Products;
import com.jason.escrap.R;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProductsFragment extends Fragment implements UserPosts.MyPostsCallback, UserPosts.GetAllPostsCallback {
    public static ProductsFragment newInstance() {
        ProductsFragment fragment = new ProductsFragment();
        return fragment;
    }

    @BindView(R.id.searchView_products)
    SearchView searchView;
    @BindView(R.id.dogs_recylerview)
    RecyclerView dogs_recycler_view;
    @BindView(R.id.txt_filter)
    TextView txt_filter;
    @BindView(R.id.empty_view)
    RelativeLayout empty_view;
    FirebaseAuth firebaseAuth;
    ProgressDialog pDialog;
    ArrayList<Products> productsList;
    PostedProductsAdapter postedProductAdapter;
    View view;
    SharedPreferences sharedPreferences;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_dogs_layout, container, false);
        ButterKnife.bind(this, view);
        productsList = new ArrayList<>();
        firebaseAuth = FirebaseAuth.getInstance();
        pDialog = new ProgressDialog(view.getContext());
        pDialog.setMessage("Please wait...");
        pDialog.setCancelable(false);
        sharedPreferences = getContext().getSharedPreferences("filter_details", Context.MODE_PRIVATE);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        UserPosts userPosts = new UserPosts(view.getContext(), this::onMyPostsResultReceived, this::onAllPostsResultReceived);
        userPosts.FetchAllPosts();


        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (!productsList.isEmpty()) {
                    postedProductAdapter.getFilter().filter(newText);
                }
                return false;
            }
        });
        txt_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                show_filter_dialog();
            }
        });

    }

    @Override
    public void onStart() {
        super.onStart();
    }


    public void show_filter_dialog() {

        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.filter_layout_ticket);
        dialog.setCancelable(true);
        CheckBox lending_availability = dialog.findViewById(R.id.checkbox_lending_availability);
        CheckBox sale_availability = dialog.findViewById(R.id.checkbox_sale_availability);
        EditText product_type = dialog.findViewById(R.id.edt_productType);
        EditText lending_price = dialog.findViewById(R.id.edt_lendingPrice);
        EditText sale_price = dialog.findViewById(R.id.edt_sale_price);
        RadioButton warranty_yes = dialog.findViewById(R.id.warranty_yes);
        RadioButton warranty_no = dialog.findViewById(R.id.warranty_no);
        Button apply_filter = dialog.findViewById(R.id.btn_save_filter);
        TextView reset_choice = dialog.findViewById(R.id.reset_choices);

        if (sharedPreferences.getString("lending_availability_status", "").equals("yes")) {
            lending_availability.setChecked(true);
        } else {
            lending_availability.setChecked(false);
        }
        if (sharedPreferences.getString("sales_availability_status", "").equals("yes")) {
            sale_availability.setChecked(true);
        } else {
            sale_availability.setChecked(false);
        }
        if (!sharedPreferences.getString("product_lending_price", "").isEmpty()) {
            lending_price.setText(sharedPreferences.getString("product_lending_price", ""));
        }
        if (!sharedPreferences.getString("product_sale_price", "").isEmpty()) {
            sale_price.setText(sharedPreferences.getString("product_sale_price", ""));
        }

        if (sharedPreferences.getString("warranty", "").equals("yes")) {
            warranty_yes.setChecked(true);
        } else if (sharedPreferences.getString("warranty", "").equals("no")) {
            warranty_no.setChecked(true);
        } else {
            warranty_yes.setChecked(false);
            warranty_no.setChecked(false);
        }

        dialog.show();


        apply_filter.setOnClickListener(new View.OnClickListener() {
            String lendingAvailability, saleAvailability, productType, lendingPrice, salePrice, warranty;

            @Override
            public void onClick(View v) {
                if (lending_availability.isChecked()) {
                    lendingAvailability = "yes";
                } else {
                    lendingAvailability = "no";
                }
                if (sale_availability.isChecked()) {
                    saleAvailability = "yes";
                } else {
                    saleAvailability = "no";
                }
                if (!product_type.getText().toString().isEmpty()) {
                    productType = product_type.getText().toString();
                } else {
                    productType = "";
                }
                if (!lending_price.getText().toString().isEmpty()) {
                    lendingPrice = lending_price.getText().toString();
                } else {
                    lendingPrice = "";
                }
                if (!sale_price.getText().toString().isEmpty()) {
                    salePrice = sale_price.getText().toString();
                } else {
                    salePrice = "";
                }
                if (warranty_yes.isChecked()) {
                    warranty = "yes";
                } else if (warranty_no.isChecked()) {
                    warranty = "no";
                } else {
                    warranty = "no_selection";
                }

                postedProductAdapter.performFiltering(lendingAvailability, saleAvailability, productType, lendingPrice, salePrice, warranty);
                dialog.dismiss();
            }
        });

        reset_choice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postedProductAdapter.performFiltering("no", "no", "", "", "", "no_selection");
                lending_availability.setChecked(false);
                sale_availability.setChecked(false);
                product_type.setText("");
                sale_price.setText("");
                lending_price.setText("");
                warranty_yes.setChecked(false);
                warranty_no.setChecked(false);
            }
        });


    }

    @Override
    public void onAllPostsResultReceived(JSONObject jsonObject) {
        JSONArray jsonArray = null;
        try {
            jsonArray = jsonObject.getJSONArray("data");
            JSONObject dataObject = null;
            for (int i = 0; i < jsonArray.length(); i++) {
                dataObject = jsonArray.getJSONObject(i);
                String briefhistory = dataObject.getString("briefhistory");
                String caption = dataObject.getString("caption");
                String expirydate = dataObject.getString("expirydate");
                String imagepath = dataObject.getString("imagepath");
                String lattitude = dataObject.getString("lattitude");
                String lendavailability = dataObject.getString("lendavailability");
                String lendavailabilityprice = dataObject.getString("lendavailabilityprice");
                String longitude = dataObject.getString("longitude");
                String manufacturingdate = dataObject.getString("manufacturingdate");
                String producttype = dataObject.getString("producttype");
                String saleavailability = dataObject.getString("saleavailability");
                String saleavailabilityprice = dataObject.getString("saleavailabilityprice");
                String type = dataObject.getString("type");
                String uid = dataObject.getString("uid");
                String warranty = dataObject.getString("warranty");
                if (!firebaseAuth.getUid().equals(uid)) {
                    productsList.add(new Products(type, caption, producttype, manufacturingdate,
                            expirydate, briefhistory, warranty, lendavailability, lendavailabilityprice, saleavailability, saleavailabilityprice, imagepath,
                            uid, Double.parseDouble(lattitude), Double.parseDouble(longitude)));
                }
            }
            //postedProductAdapter.notifyDataSetChanged();
            if (!productsList.isEmpty()) {
                dogs_recycler_view.setVisibility(View.VISIBLE);
                empty_view.setVisibility(View.GONE);
                postedProductAdapter = new PostedProductsAdapter(view.getContext(), productsList);
                dogs_recycler_view.setLayoutManager(new GridLayoutManager(getContext(), 1));
                dogs_recycler_view.setAdapter(postedProductAdapter);
            } else {
                dogs_recycler_view.setVisibility(View.GONE);
                empty_view.setVisibility(View.VISIBLE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onMyPostsResultReceived(JSONObject jsonObject) {

    }
}
