package com.jason.escrap.Fragments.HomeFragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.libraries.places.api.Places;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.jason.escrap.Adapter.HomeProductAdapter;
import com.jason.escrap.Database.UserPosts;
import com.jason.escrap.Model.Products;
import com.jason.escrap.R;
import com.jason.escrap.Utils.Utils;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeFragment extends Fragment implements OnMapReadyCallback, UserPosts.MyPostsCallback, UserPosts.GetAllPostsCallback {
    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        return fragment;
    }

    View view;
    MapView mapView;
    GoogleMap gMap;
    @BindView(R.id.home_dogs_recylerview)
    RecyclerView recyclerView;
    FirebaseAuth firebaseAuth;
    ProgressDialog pDialog;
    ArrayList<Products> productsList;
    HomeProductAdapter homeProductAdapter;
    @BindView(R.id.searchView_dogs)
    SearchView searchView;
    @BindView(R.id.txt_filter)
    TextView txt_filter;
    @BindView(R.id.empty_source)
    ImageView empty_source;
    @BindView(R.id.empty_t1)
    TextView empty_t1;
    @BindView(R.id.empty_t2)
    TextView empty_t2;
    SharedPreferences sharedPreferences;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home_layout, container, false);
        ButterKnife.bind(this, view);
        mapView = view.findViewById(R.id.google_map_fragment);
        productsList = new ArrayList<>();
        firebaseAuth = FirebaseAuth.getInstance();
        pDialog = new ProgressDialog(view.getContext());
        pDialog.setMessage("Please wait...");
        pDialog.setCancelable(false);
        sharedPreferences = getContext().getSharedPreferences("filter_details", Context.MODE_PRIVATE);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (!Places.isInitialized()) {
            Places.initialize(getContext(), Utils.MapKey);
        }
        MapsInitializer.initialize(view.getContext());
        mapView.onCreate(savedInstanceState);
        mapView.onResume();
        mapView.getMapAsync(this);


        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (!productsList.isEmpty()) {
                    homeProductAdapter.getFilter().filter(newText);
                }
                return false;
            }
        });
        txt_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                show_filter_dialog();
            }
        });


    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        gMap = googleMap;
        gMap.clear();
        pDialog.show();
        UserPosts userPosts = new UserPosts(view.getContext(), this::onMyPostsResultReceived, this::onAllPostsResultReceived);
        userPosts.FetchAllPosts();
    }

    public void retrieveLocations() {
        pDialog.show();
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child("Posts");
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                productsList.clear();
                String uid = firebaseAuth.getUid();
                for (DataSnapshot ds : snapshot.getChildren()) {
                    Products products = ds.getValue(Products.class);
                    if (!products.getUid().equals(uid)) {
                        productsList.add(products);
                    }
                }
                for (int i = 0; i < productsList.size(); i++) {
                    LatLng latLng = new LatLng(productsList.get(i).getLattitude(), productsList.get(i).getLongitude());
                    gMap.addMarker(new MarkerOptions().position(latLng).title("Hey!").icon(BitmapDescriptorFactory.fromResource(R.drawable.pin_blue)));
                    gMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 2));
                }
                pDialog.dismiss();
                homeProductAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    public void show_filter_dialog() {

        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.filter_layout_ticket);
        dialog.setCancelable(true);
        CheckBox lending_availability = dialog.findViewById(R.id.checkbox_lending_availability);
        CheckBox sale_availability = dialog.findViewById(R.id.checkbox_sale_availability);
        EditText product_type = dialog.findViewById(R.id.edt_productType);
        EditText lending_price = dialog.findViewById(R.id.edt_lendingPrice);
        EditText sale_price = dialog.findViewById(R.id.edt_sale_price);
        RadioButton warranty_yes = dialog.findViewById(R.id.warranty_yes);
        RadioButton warranty_no = dialog.findViewById(R.id.warranty_no);
        Button apply_filter = dialog.findViewById(R.id.btn_save_filter);
        TextView reset_choice = dialog.findViewById(R.id.reset_choices);

        if (sharedPreferences.getString("lending_availability_status", "").equals("yes")) {
            lending_availability.setChecked(true);
        } else {
            lending_availability.setChecked(false);
        }
        if (sharedPreferences.getString("sales_availability_status", "").equals("yes")) {
            sale_availability.setChecked(true);
        } else {
            sale_availability.setChecked(false);
        }
        if (!sharedPreferences.getString("product_lending_price", "").isEmpty()) {
            lending_price.setText(sharedPreferences.getString("product_lending_price", ""));
        }
        if (!sharedPreferences.getString("product_sale_price", "").isEmpty()) {
            sale_price.setText(sharedPreferences.getString("product_sale_price", ""));
        }

        if (sharedPreferences.getString("warranty", "").equals("yes")) {
            warranty_yes.setChecked(true);
        } else if (sharedPreferences.getString("warranty", "").equals("no")) {
            warranty_no.setChecked(true);
        } else {
            warranty_yes.setChecked(false);
            warranty_no.setChecked(false);
        }

        dialog.show();


        apply_filter.setOnClickListener(new View.OnClickListener() {
            String lendingAvailability, saleAvailability, productType, lendingPrice, salePrice, warranty;

            @Override
            public void onClick(View v) {
                if (lending_availability.isChecked()) {
                    lendingAvailability = "yes";
                } else {
                    lendingAvailability = "no";
                }
                if (sale_availability.isChecked()) {
                    saleAvailability = "yes";
                } else {
                    saleAvailability = "no";
                }
                if (!product_type.getText().toString().isEmpty()) {
                    productType = product_type.getText().toString();
                } else {
                    productType = "";
                }
                if (!lending_price.getText().toString().isEmpty()) {
                    lendingPrice = lending_price.getText().toString();
                } else {
                    lendingPrice = "";
                }
                if (!sale_price.getText().toString().isEmpty()) {
                    salePrice = sale_price.getText().toString();
                } else {
                    salePrice = "";
                }
                if (warranty_yes.isChecked()) {
                    warranty = "yes";
                } else if (warranty_no.isChecked()) {
                    warranty = "no";
                } else {
                    warranty = "no_selection";
                }

                homeProductAdapter.performFiltering(lendingAvailability, saleAvailability, productType, lendingPrice, salePrice, warranty);
                dialog.dismiss();
            }
        });

        reset_choice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                homeProductAdapter.performFiltering("no", "no", "", "", "", "no_selection");
                lending_availability.setChecked(false);
                sale_availability.setChecked(false);
                product_type.setText("");
                sale_price.setText("");
                lending_price.setText("");
                warranty_yes.setChecked(false);
                warranty_no.setChecked(false);
            }
        });


    }


    @Override
    public void onMyPostsResultReceived(JSONObject jsonObject) {

    }

    @Override
    public void onAllPostsResultReceived(JSONObject jsonObject) {
        JSONArray jsonArray = null;
        try {
            jsonArray = jsonObject.getJSONArray("data");
            JSONObject dataObject = null;
            productsList.clear();
            for (int i = 0; i < jsonArray.length(); i++) {
                dataObject = jsonArray.getJSONObject(i);
                String briefhistory = dataObject.getString("briefhistory");
                String caption = dataObject.getString("caption");
                String expirydate = dataObject.getString("expirydate");
                String imagepath = dataObject.getString("imagepath");
                String lattitude = dataObject.getString("lattitude");
                String lendavailability = dataObject.getString("lendavailability");
                String lendavailabilityprice = dataObject.getString("lendavailabilityprice");
                String longitude = dataObject.getString("longitude");
                String manufacturingdate = dataObject.getString("manufacturingdate");
                String producttype = dataObject.getString("producttype");
                String saleavailability = dataObject.getString("saleavailability");
                String saleavailabilityprice = dataObject.getString("saleavailabilityprice");
                String type = dataObject.getString("type");
                String uid = dataObject.getString("uid");
                String warranty = dataObject.getString("warranty");
                //don't show logged in user details
                if (!firebaseAuth.getUid().equals(uid)) {
                    productsList.add(new Products(type, caption, producttype, manufacturingdate,
                            expirydate, briefhistory, warranty, lendavailability, lendavailabilityprice, saleavailability, saleavailabilityprice, imagepath,
                            uid, Double.parseDouble(lattitude), Double.parseDouble(longitude)));
                }
            }
            for (int i = 0; i < productsList.size(); i++) {
                LatLng latLng = new LatLng(productsList.get(i).getLattitude(), productsList.get(i).getLongitude());
                gMap.addMarker(new MarkerOptions().position(latLng).title("Hii!").icon(BitmapDescriptorFactory.fromResource(R.drawable.pin_blue)));
                gMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 2));
            }
            pDialog.dismiss();
            //homeProductAdapter.notifyDataSetChanged();
            if (!productsList.isEmpty()) {
                recyclerView.setVisibility(View.VISIBLE);
                empty_source.setVisibility(View.GONE);
                empty_t1.setVisibility(View.GONE);
                empty_t2.setVisibility(View.GONE);
                homeProductAdapter = new HomeProductAdapter(view.getContext(), productsList);
                recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));
                recyclerView.setAdapter(homeProductAdapter);
            } else {
                recyclerView.setVisibility(View.GONE);
                empty_source.setVisibility(View.VISIBLE);
                empty_t1.setVisibility(View.VISIBLE);
                empty_t2.setVisibility(View.VISIBLE);
            }

        } catch (JSONException e) {

        }
    }
}