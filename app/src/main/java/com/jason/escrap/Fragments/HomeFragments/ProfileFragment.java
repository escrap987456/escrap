package com.jason.escrap.Fragments.HomeFragments;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.jason.escrap.Activity.AddPostActivity;
import com.jason.escrap.Adapter.MyProductPostAdapter;
import com.jason.escrap.Database.ProfileData;
import com.jason.escrap.Database.UpdateProfileImage;
import com.jason.escrap.Database.UserPosts;
import com.jason.escrap.Model.Products;
import com.jason.escrap.R;
import com.jason.escrap.Utils.Utils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import androidx.annotation.ColorRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ProfileFragment extends Fragment implements ProfileData.TestTaskCallback, UserPosts.MyPostsCallback, UserPosts.GetAllPostsCallback {
    public static ProfileFragment newInstance() {
        ProfileFragment fragment = new ProfileFragment();
        return fragment;
    }

    @BindView(R.id.btn_add_post)
    Button btn_add_post;
    @BindView(R.id.my_posts_recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.user_profile_name)
    TextView user_profile_name;
    @BindView(R.id.user_product_owner)
    TextView userprofile_owner;
    @BindView(R.id.user_product_seller)
    TextView userprofile_seller;
    @BindView(R.id.user_general)
    TextView userprofile_genral;
    @BindView(R.id.user_profile_email)
    TextView userprofile_email;
    @BindView(R.id.user_profile_phone)
    TextView userprofile_phone;
    @BindView(R.id.user_profile_address)
    TextView userprofile_address;
    @BindView(R.id.user_profile_brief_intro)
    TextView userprofile_brief_intro;
    @BindView(R.id.swipe_refresh_profile)
    SwipeRefreshLayout mSwipeRefreshLayout;
    @BindView(R.id.btn_update_profile)
    Button btn_update_profile;
    @BindView(R.id.btn_update_cover_image)
    ImageView btn_update_cover;
    @BindView(R.id.cover_image)
    ImageView cover_image;
    @BindView(R.id.btn_set_cover_image)
    ImageView btn_set_cover_image;
    @BindView(R.id.update_dp)
    ImageView update_dp;
    @BindView(R.id.dp)
    ImageView dp;

    MyProductPostAdapter myProductPostAdapter;
    ArrayList<Products> productsList;
    View view;
    FirebaseAuth firebaseAuth;
    ProgressDialog pDialog;
    JSONObject jsonObject;
    Dialog dialog;
    ProfileData profileData;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_profile_layout, container, false);
        ButterKnife.bind(this, view);
        productsList = new ArrayList<>();
        firebaseAuth = FirebaseAuth.getInstance();
        pDialog = new ProgressDialog(view.getContext());
        pDialog.setMessage("Please wait...");
        pDialog.setCancelable(false);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        profileData = new ProfileData(firebaseAuth.getUid(), this::onResultReceived);
        profileData.getProfileInformation();

        UserPosts userPosts = new UserPosts(view.getContext(), this::onMyPostsResultReceived, this::onAllPostsResultReceived);
        userPosts.FetchMyPosts();


        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                productsList.clear();
                userPosts.FetchMyPosts();
                profileData.getProfileInformation();
                myProductPostAdapter.notifyDataSetChanged();
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });


        btn_add_post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(view.getContext(), AddPostActivity.class));
            }
        });
        myProductPostAdapter = new MyProductPostAdapter(view.getContext(), productsList);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 1));
        recyclerView.setAdapter(myProductPostAdapter);

        btn_update_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //show update profile dialog
                show_update_profile_dialog();
            }
        });

        btn_update_cover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, 123);
            }
        });
        update_dp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, 456);
            }
        });


    }

    private void show_update_profile_dialog() {
        dialog = new Dialog(getContext(), R.style.DialogTheme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#FFFFFF")));
        dialog.setContentView(R.layout.update_profile_ticket);
        dialog.setCancelable(true);
        CheckBox checkBox_general_user = dialog.findViewById(R.id.checkbox_general_user);
        CheckBox checkBox_product_owner = dialog.findViewById(R.id.checkbox_product_owner);
        CheckBox checkBox_product_seller = dialog.findViewById(R.id.checkbox_product_seller);
        EditText edt_name = dialog.findViewById(R.id.edt_name);
        EditText edt_phone = dialog.findViewById(R.id.edt_phone);
        EditText edt_address = dialog.findViewById(R.id.edt_address);
        EditText edt_description = dialog.findViewById(R.id.edt_brief);
        Button btn_update_profile = dialog.findViewById(R.id.btn_update_user_profile);

        if (jsonObject != null) {
            try {
                edt_name.setText(jsonObject.getString("name"));
                if (jsonObject.getString("productowner").equals("1")) {
                    checkBox_product_owner.setChecked(true);
                } else {
                    checkBox_product_owner.setChecked(false);
                }
                if (jsonObject.getString("productseller").equals("1")) {
                    checkBox_product_seller.setChecked(true);
                } else {
                    checkBox_product_seller.setChecked(false);
                }
                if (jsonObject.getString("general").equals("1")) {
                    checkBox_general_user.setChecked(true);
                } else {
                    checkBox_general_user.setChecked(false);
                }
                if (!jsonObject.getString("phoneno").isEmpty()) {
                    edt_phone.setText(jsonObject.getString("phoneno"));
                } else {
                    edt_phone.setText("");
                }

                if (!jsonObject.getString("address").isEmpty()) {
                    edt_address.setText(jsonObject.getString("address"));
                } else {
                    edt_address.setText("N/A");
                }
                if (!jsonObject.getString("description").isEmpty()) {
                    edt_description.setText(jsonObject.getString("description"));
                } else {
                    edt_description.setText("N/A");
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }


        btn_update_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkValidity(edt_name, edt_phone, edt_address, edt_description)) {
                    pDialog.show();
                    String general = "", owner = "", seller = "";
                    if (checkBox_general_user.isChecked()) {
                        general = "1";
                    } else {
                        general = "0";
                    }
                    if (checkBox_product_owner.isChecked()) {
                        owner = "1";
                    } else {
                        owner = "0";
                    }
                    if (checkBox_product_seller.isChecked()) {
                        seller = "1";
                    } else {
                        seller = "0";
                    }
                    new Update_user_info().execute(edt_address.getText().toString()
                            , edt_description.getText().toString(),
                            general, edt_name.getText().toString(), edt_phone.getText().toString(),
                            owner, seller, firebaseAuth.getUid());
                }
            }
        });
        dialog.show();

    }

    public boolean checkValidity(EditText edt_name, EditText edt_phone, EditText edt_address, EditText edt_description) {
        if (edt_name.getText().toString().isEmpty()) {
            Toast.makeText(view.getContext(), "Name should not be empty", Toast.LENGTH_SHORT).show();
            return false;
        } else if (edt_phone.getText().toString().isEmpty()) {
            Toast.makeText(view.getContext(), "Phone number should not be empty", Toast.LENGTH_SHORT).show();
            return false;
        } else if (edt_address.getText().toString().isEmpty()) {
            Toast.makeText(view.getContext(), "Address should not be empty", Toast.LENGTH_SHORT).show();
            return false;
        } else if (edt_description.getText().toString().isEmpty()) {
            Toast.makeText(view.getContext(), "Description should not be empty", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResultReceived(JSONObject result) {
//        Log.d("TagData", result.toString());
        this.jsonObject = result;
        if (result != null) {
            try {
                user_profile_name.setText(result.getString("name"));
                if (result.getString("productowner").equals("1")) {
                    userprofile_owner.setText("Product Owner");
                } else {
                    userprofile_owner.setText("");
                }
                if (result.getString("productseller").equals("1")) {
                    userprofile_seller.setText(",Product Seller");
                } else {
                    userprofile_seller.setText("");
                }
                if (result.getString("general").equals("1")) {
                    userprofile_genral.setText(",General customer");
                } else {
                    userprofile_genral.setText("");
                }
                userprofile_email.setText(result.getString("email"));
                if (!result.getString("phoneno").isEmpty()) {
                    userprofile_phone.setText(result.getString("phoneno"));
                } else {
                    userprofile_phone.setText("N/A");
                }

                if (!result.getString("address").isEmpty()) {
                    userprofile_address.setText(result.getString("address"));
                } else {
                    userprofile_address.setText("N/A");
                }
                if (!result.getString("description").isEmpty()) {
                    userprofile_brief_intro.setText(result.getString("description"));
                } else {
                    userprofile_brief_intro.setText("N/A");
                }

                if (!result.isNull("coverImage")) {
                    Glide.with(view.getContext()).load(Utils.FETCH_COVER_IMAGE_URL + result.getString("coverImage"))
                            .into(cover_image);
                }
                if (!result.isNull("profileImage")) {
                    Glide.with(view.getContext()).load(Utils.FETCH_DP_IMAGE_URL + result.getString("profileImage"))
                            .circleCrop().into(dp);
                }


            } catch (JSONException e) {
                Toast.makeText(view.getContext(), "Unknown error occurred while fetching profile info", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onMyPostsResultReceived(JSONObject jsonObject) {

        JSONArray jsonArray = null;
        try {
            jsonArray = jsonObject.getJSONArray("data");
            JSONObject dataObject = null;
            for (int i = 0; i < jsonArray.length(); i++) {
                dataObject = jsonArray.getJSONObject(i);
                String briefhistory = dataObject.getString("briefhistory");
                String caption = dataObject.getString("caption");
                String expirydate = dataObject.getString("expirydate");
                String imagepath = dataObject.getString("imagepath");
                String lattitude = dataObject.getString("lattitude");
                String lendavailability = dataObject.getString("lendavailability");
                String lendavailabilityprice = dataObject.getString("lendavailabilityprice");
                String longitude = dataObject.getString("longitude");
                String manufacturingdate = dataObject.getString("manufacturingdate");
                String producttype = dataObject.getString("producttype");
                String saleavailability = dataObject.getString("saleavailability");
                String saleavailabilityprice = dataObject.getString("saleavailabilityprice");
                String type = dataObject.getString("type");
                String uid = dataObject.getString("uid");
                String warranty = dataObject.getString("warranty");
                productsList.add(new Products(type, caption, producttype, manufacturingdate,
                        expirydate, briefhistory, warranty, lendavailability, lendavailabilityprice, saleavailability, saleavailabilityprice, imagepath,
                        uid, Double.parseDouble(lattitude), Double.parseDouble(longitude)));
            }
            myProductPostAdapter.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onAllPostsResultReceived(JSONObject jsonObject) {

    }

    public class Update_user_info extends AsyncTask<String, Integer, Response> {

        @Override
        protected Response doInBackground(String... strings) {
            OkHttpClient client = new OkHttpClient().newBuilder()
                    .build();
            MediaType mediaType = MediaType.parse("text/plain");
            RequestBody body = new MultipartBody.Builder().setType(MultipartBody.FORM)
                    .addFormDataPart("address", strings[0])
                    .addFormDataPart("description", strings[1])
                    .addFormDataPart("general", strings[2])
                    .addFormDataPart("name", strings[3])
                    .addFormDataPart("phoneno", strings[4])
                    .addFormDataPart("productowner", strings[5])
                    .addFormDataPart("productseller", strings[6])
                    .addFormDataPart("uid", strings[7])
                    .build();
            Request request = new Request.Builder()
                    .url(Utils.UPDATE_PROFILE_URL)
                    .method("POST", body)
                    .build();
            Response response = null;
            try {
                response = client.newCall(request).execute();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response;
        }

        @Override
        protected void onPostExecute(Response response) {
            super.onPostExecute(response);

            if (response != null && response.code() == 200) {
                dialog.dismiss();
                pDialog.dismiss();
                profileData.getProfileInformation();
                Toast.makeText(view.getContext(), "Profile Updated Successfully", Toast.LENGTH_SHORT).show();
            } else {
                dialog.dismiss();
                pDialog.dismiss();
                Toast.makeText(view.getContext(), "Network error occurred", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 123) {
            try {
                final Uri imageUri = data.getData();
                final InputStream imageStream = view.getContext().getContentResolver().openInputStream(imageUri);
                final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                cover_image.setImageBitmap(selectedImage);
                String picturePath = getPath(view.getContext(), imageUri);

                //updating cover image
                UpdateProfileImage updateProfileImage = new UpdateProfileImage(view.getContext());
                updateProfileImage.updateProfileCover(firebaseAuth.getUid(), picturePath);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
                Toast.makeText(view.getContext(), "Something went wrong", Toast.LENGTH_LONG).show();
            }

        } else if (requestCode == 456) {
            try {
                final Uri imageUri = data.getData();
                final InputStream imageStream = view.getContext().getContentResolver().openInputStream(imageUri);
                final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                //dp.setImageBitmap(selectedImage);
                //btn_set_cover_image.setVisibility(View.VISIBLE);
                Glide.with(view.getContext()).load(selectedImage)
                        .circleCrop().into(dp);

                String picturePath = getPath(view.getContext(), imageUri);

                //updating cover image
                UpdateProfileImage updateProfileImage = new UpdateProfileImage(view.getContext());
                updateProfileImage.updateProfileDP(firebaseAuth.getUid(), picturePath);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
                Toast.makeText(view.getContext(), "Something went wrong", Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(view.getContext(), "You haven't picked any Image", Toast.LENGTH_LONG).show();
        }
    }

    public static String getPath(Context context, Uri uri) {
        String result = null;
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = context.getContentResolver().query(uri, proj, null, null, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                int column_index = cursor.getColumnIndexOrThrow(proj[0]);
                result = cursor.getString(column_index);
            }
            cursor.close();
        }
        if (result == null) {
            result = "Not found";
        }
        return result;
    }


}
